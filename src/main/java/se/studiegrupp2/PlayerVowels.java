package se.studiegrupp2;


public class PlayerVowels implements Player{
    private String name="Wowels";
    private int score;
    private int standing;
    private Moves move;
    private String opponentName;
    private boolean isHuman = false;

    @Override
    public Moves makeAMove() {
        int move=opponentName.toLowerCase().replaceAll("[^ aeiouåäöyAEIOUYÅÄÖ]","").length();
        if (move > 3) move = 3;
        switch(move){
            case 1 -> this.move = Moves.ROCK;
            case 2 -> this.move = Moves.PAPER;
            case 3 -> this.move = Moves.SCISSORS;
        }
        return this.move;
    }
    public void getOpponentName(String opponentName){
        this.opponentName=opponentName;
    }
    @Override
    public void incrementScore() {
        score++;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public int getScore() {
        return score;
    }

    @Override
    public void setStanding(int standing) {
        this.standing = standing;
    }
    @Override
    public int getStanding() {
        return standing;
    }

    @Override
    public boolean isHuman() {
        return isHuman;
    }
}
