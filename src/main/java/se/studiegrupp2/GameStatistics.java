package se.studiegrupp2;

import java.util.ArrayList;
import java.util.List;

public class GameStatistics {

private final List<String> scores = new ArrayList<>();
private final List<Player> standingList = new ArrayList<>();

public List<String> getScoreList() {
    return scores;
}

public void addToScoreList(String result) {
    scores.add(result);
}

public List<Player> getStandingList() {
    return standingList;
}

public void addToStandingList(Player player) {
    standingList.add(player);
}

}
