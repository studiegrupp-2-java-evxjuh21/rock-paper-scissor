package se.studiegrupp2;

import java.util.Scanner;

public class Main {

    static GameStatistics gameStatistics = new GameStatistics();
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {


        String menuSelection;
        boolean isRunning = true;
        System.out.println("Welcome to Rock Paper Scissors!");
        System.out.println("Please enter your name: ");
        String playerName = scanner.nextLine();
        System.out.println();
        System.out.println("|--- Welcome " + playerName + "! ---|");


        while (isRunning) {

            System.out.println("[1] Play game");
            System.out.println("[2] Show statistics");
            System.out.println("[3] Exit");
            menuSelection = scanner.nextLine();
            switch (menuSelection) {
                case "1" -> new Game(playerName, gameStatistics).newGame();
                case "2" -> statisticsMenu();
                case "3" -> {
                    System.out.println("Exiting game..");
                    isRunning = false;
                }
                default -> System.out.println("Invalid menu selection");
            }
        }
    }

    public static void statisticsMenu() {
        boolean subMenuIsActive = true;
        while (subMenuIsActive) {
            System.out.println("----- STATISTICS -----");
            System.out.println("[1] Results form all matches");
            System.out.println("[2] Show your stats");
            System.out.println("[3] Back to main menu");
            String menuSelection = scanner.nextLine();
            switch (menuSelection) {
                case "1" -> allMatches();
                case "2" -> allStats();
                case "3" -> subMenuIsActive = false;
                default -> System.out.println("Invalid menu selection");
            }
        }
    }

    public static void allMatches() {
        if (gameStatistics.getScoreList().isEmpty()) {
            System.out.println("No statistics to show, play a game first.");
        } else{
            gameStatistics.getScoreList().forEach(System.out::println);
        }
    }

    public static void allStats() {
        if (gameStatistics.getStandingList().isEmpty()) {
            System.out.println("No statistics to show, play a game first.");
        } else {
            System.out.println(
                    "Your average standing: " +
                            (int) gameStatistics.getStandingList()
                                    .stream()
                                    .mapToInt(Player::getStanding)
                                    .summaryStatistics()
                                    .getAverage()
            );
            System.out.println(
                    "Your best standing: " +
                            gameStatistics.getStandingList()
                                    .stream()
                                    .mapToInt(Player::getStanding)
                                    .summaryStatistics()
                                    .getMin()
            );
            System.out.println(
                    "Your worst standing: " +
                            gameStatistics.getStandingList()
                                    .stream()
                                    .mapToInt(Player::getStanding)
                                    .summaryStatistics()
                                    .getMax()
            );
        }
    }
}
