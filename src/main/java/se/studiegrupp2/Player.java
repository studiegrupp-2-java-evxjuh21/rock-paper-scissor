package se.studiegrupp2;

public interface Player {

    Moves makeAMove();
    void incrementScore();
    void setName(String name);
    String getName();
    int getScore();
    void setStanding(int standing);
    int getStanding();
    boolean isHuman();


}
