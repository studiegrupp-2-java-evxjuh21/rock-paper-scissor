package se.studiegrupp2;

import java.time.LocalTime;

public class PlayerTime implements Player {
    private String name = "Time Man";
    private int score;
    private int standing;
    private boolean isHuman = false;

    @Override
    public Moves makeAMove() {
        LocalTime localTime = LocalTime.now();
        long seconds = localTime.getNano() % 1000 / 100;
        if (seconds <= 2) return Moves.PAPER;
        if (seconds <= 5) return Moves.ROCK;
        return Moves.SCISSORS;
    }


    @Override
    public void incrementScore() {
        score++;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public int getScore() {
        return score;
    }

    @Override
    public void setStanding(int standing) {
        this.standing = standing;
    }

    @Override
    public int getStanding() {
        return standing;
    }

    @Override
    public boolean isHuman() {
        return isHuman;
    }
}
