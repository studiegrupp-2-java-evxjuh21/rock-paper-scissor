package se.studiegrupp2;

import java.util.Scanner;

public class PlayerUser implements Player{
    private String name;
    private int score;
    private int standing;
    private final Scanner scanner = new Scanner(System.in);
    private boolean isHuman = true;

    @Override
    public Moves makeAMove() {
        System.out.println("Your move: ");
        System.out.println("1. Rock");
        System.out.println("2. Paper");
        System.out.println("3. Scissors");
        String playerMove = scanner.nextLine();
        return switch(playerMove){
            case "1" -> Moves.ROCK;
            case "2" -> Moves.PAPER;
            case "3" -> Moves.SCISSORS;
            default -> makeAMove();
        };
    }

    @Override
    public void incrementScore() {
        score++;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public int getScore() {
        return score;
    }

    @Override
    public void setStanding(int standing) {
        this.standing = standing;
    }

    @Override
    public int getStanding() {
        return standing;
    }

    @Override
    public boolean isHuman() {
        return isHuman;
    }

}
