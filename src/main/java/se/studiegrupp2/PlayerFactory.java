package se.studiegrupp2;
//Här har vi använt design pattern: Simple Factory pattern
public class PlayerFactory {

    public Player createPlayer(PlayerPersonality playerPersonality) {
        return switch(playerPersonality){
            case RANDOM -> new PlayerRandom();
            case TIME -> new PlayerTime();
            case VOWEL -> new PlayerVowels();
            case USER -> new PlayerUser();
        };
    }

}
