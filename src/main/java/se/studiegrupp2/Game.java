package se.studiegrupp2;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Game {


    private final String playerName;
    private final GameStatistics gameStatistics;


    public Game(String playerName, GameStatistics gameStatistics) {
        this.playerName = playerName;
        this.gameStatistics = gameStatistics;
    }
//Här har vi använt design pattern: Simple Factory pattern
    public void newGame() {
        PlayerFactory playerFactory = new PlayerFactory();
        Player playerUser = playerFactory.createPlayer(PlayerPersonality.USER);
        playerUser.setName(playerName);
        Player playerRandom = playerFactory.createPlayer(PlayerPersonality.RANDOM);
        Player playerVowels = playerFactory.createPlayer(PlayerPersonality.VOWEL);
        Player playerTime = playerFactory.createPlayer(PlayerPersonality.TIME);

        List<Player> playerList = new ArrayList<>(List.of(playerUser, playerRandom, playerTime, playerVowels));



        // Round 1
        whoWon(playerUser, playerRandom, playerUser.makeAMove(), playerRandom.makeAMove());
        ((PlayerVowels) playerVowels).getOpponentName(playerTime.getName());
        whoWon(playerVowels, playerTime, playerVowels.makeAMove(), playerTime.makeAMove());

        // Round 2
        whoWon(playerUser, playerTime, playerUser.makeAMove(), playerTime.makeAMove());
        ((PlayerVowels) playerVowels).getOpponentName(playerRandom.getName());
        whoWon(playerVowels, playerRandom, playerRandom.makeAMove(), playerTime.makeAMove());

        // Round 3
        ((PlayerVowels) playerVowels).getOpponentName(playerUser.getName());
        whoWon(playerUser, playerVowels, playerUser.makeAMove(), playerVowels.makeAMove());
        whoWon(playerRandom, playerTime, playerRandom.makeAMove(), playerTime.makeAMove());

        addStats(playerList);

    }

    private void whoWon(Player player, Player opponent, Moves playerMove, Moves opponentMove) {
        switch (playerMove) {
            case ROCK -> {
                if (opponentMove.equals(Moves.SCISSORS))
                    playerWin(player, opponent, playerMove, opponentMove);

                if (opponentMove.equals(Moves.PAPER))
                    playerLose(player, opponent, playerMove, opponentMove);

                if (opponentMove.equals(Moves.ROCK))
                    tie(player, opponent);
            }
            case SCISSORS -> {
                if (opponentMove.equals(Moves.ROCK))
                    playerLose(player, opponent, playerMove, opponentMove);

                if (opponentMove.equals(Moves.PAPER))
                    playerWin(player, opponent, playerMove, opponentMove);

                if (opponentMove.equals(Moves.SCISSORS))
                    tie(player, opponent);
            }
            case PAPER -> {
                if (opponentMove.equals(Moves.SCISSORS))
                    playerLose(player, opponent, playerMove, opponentMove);

                if (opponentMove.equals(Moves.ROCK))
                    playerWin(player, opponent, playerMove, opponentMove);

                if (opponentMove.equals(Moves.PAPER))
                    tie(player, opponent);
            }
        }
    }

    private void addStats(List<Player> playerList) {
        LocalDateTime localDateTime = LocalDateTime.now();
        DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        String result = "Result from " + localDateTime.format(format) + ": ";

        AtomicInteger count = new AtomicInteger(0);

        result += playerList
                .stream()
                .sorted(Comparator.comparing(Player::getScore).reversed())
                .map(player -> count.incrementAndGet() + ". " + player.getName())
                .collect(Collectors.joining(", "));

        gameStatistics.addToScoreList(result);

        playerList.sort(Comparator.comparing(Player::getScore).reversed());

        for (int i = 0; i < playerList.size(); i++) {
            if (playerList.get(i).isHuman()) {
                playerList.get(i).setStanding(i + 1);
                gameStatistics.addToStandingList(playerList.get(i));
            }
        }
    }

    private void playerWin(Player player, Player opponent, Moves playerMove, Moves opponentMove) {
        if (player.isHuman()) {
            System.out.println(
                    player.getName() + "/" + playerMove +
                            " vs " + opponent.getName() + "/" + opponentMove
                            + ". You win! :)")
            ;
        }
        player.incrementScore();

    }

    private void playerLose(Player player, Player opponent, Moves playerMove, Moves opponentMove) {
        if (player.isHuman()) {
            System.out.println(
                    player.getName() + "/" + playerMove +
                            " vs " + opponent.getName() + "/" + opponentMove +
                            ". You loose! :("
            );
        }
        opponent.incrementScore();

    }

    private void tie(Player player, Player opponent) {
        if (player.isHuman()) {
            System.out.println("Tie! Play again");
        }
        whoWon(player, opponent, player.makeAMove(), opponent.makeAMove());
    }

}
