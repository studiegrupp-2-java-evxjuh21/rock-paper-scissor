package se.studiegrupp2;

public class PlayerRandom implements Player {
    private String name = "Randy";
    private int score;
    private int standing;
    private boolean isHuman = false;


    @Override
    public Moves makeAMove() {
        int move = (int) Math.floor((Math.random() * 3) + 1);
        return switch (move) {
            case 1 -> Moves.ROCK;
            case 2 -> Moves.PAPER;
            case 3 -> Moves.SCISSORS;
            default -> makeAMove();
        };
    }

    @Override
    public void incrementScore() {
        score++;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public int getScore() {
        return score;
    }

    @Override
    public void setStanding(int standing) {
        this.standing = standing;
    }

    @Override
    public int getStanding() {
        return standing;
    }

    @Override
    public boolean isHuman() {
        return isHuman;
    }

}
